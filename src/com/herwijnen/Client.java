package com.herwijnen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Client {

    private static List<String> WEAPONS = new ArrayList<>(Arrays.asList("AK 47", "Gut Knife", "Desert Eagle", "Maverick"));

    static String getRandomType(){
        Random r = new Random();
        int random = r.nextInt(100);
        if(random%2==0){
            return "terrorist";
        } else {
            return "counter terrorist";
        }
    }

    static String getRandomWeapon(){
        Random r = new Random();
        return WEAPONS.get(r.nextInt(WEAPONS.size()));
    }

    public static void main(String[] args) throws Exception{

        PlayerFactory myFactory = new PlayerFactory();

        IPlayer player;
        // Create 50 random players
        for (int i = 0; i < 50; i++){
            player = myFactory.getPlayer(getRandomType());
            player.assignWeapon(getRandomWeapon());
            player.mission();
        }

        System.out.println("\nNumber of objects is " + myFactory.getMapSize());
    }
}
