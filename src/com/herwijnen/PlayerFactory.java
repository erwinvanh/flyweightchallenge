package com.herwijnen;

import java.util.HashMap;

public class PlayerFactory {
    private HashMap<String, IPlayer> players = new HashMap<>();

    public int getMapSize(){
        return players.size();
    }

    public IPlayer getPlayer(String playerType) throws Exception{
        IPlayer player = null;
        if (players.containsKey(playerType.toLowerCase())){
            player = players.get(playerType.toLowerCase());
        } else {
            switch (playerType.toLowerCase()) {
                case "terrorist":
                    player = new Terrorist();
                    System.out.println("******* Terrorist created ******* ");
                    break;
                case "counter terrorist":
                    player = new CounterTerrorist();
                    System.out.println("******* Counter-terrorist created ******* ");
                    break;
                default:
                    throw new Exception("Cant create player");
            }
            players.put(playerType.toLowerCase(), player);
        }

        return player;
    }
}
