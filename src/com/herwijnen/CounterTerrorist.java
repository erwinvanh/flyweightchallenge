package com.herwijnen;

public class CounterTerrorist implements IPlayer {
    private String weapon;
    private final String task;

    public CounterTerrorist() {
        this.task = "Defuse bomb";
    }

    @Override
    public void assignWeapon(String weapon) {
        this.weapon = weapon;
    }

    @Override
    public void mission() {
        System.out.println("Counter-terrorist with weapon " + weapon + " and task " + task);
    }
}
