package com.herwijnen;

public interface IPlayer {
    void assignWeapon(String weapon);
    void mission();
}
