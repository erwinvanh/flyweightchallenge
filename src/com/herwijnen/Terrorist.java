package com.herwijnen;

public class Terrorist implements IPlayer {
    // Intrinsic data (set upon creation)
    private final String task;
    // Extrinsic data (added per instance)
    private String weapon;

    public Terrorist() {
        this.task = "Plant a bomb";
    }

    @Override
    public void assignWeapon(String weapon) {
        this.weapon = weapon;
    }

    @Override
    public void mission() {
        System.out.println("Terrorist with weapon " + weapon + " and task " + task);
    }
}
